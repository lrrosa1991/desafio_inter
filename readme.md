## Para compilar e rodar a aplicação

- Para compilar
```
    mvn clean install
```

- Para executar a aplicação

```
    cd target && java -jar desafio_inter-0.0.1-SNAPSHOT.jar
``` 