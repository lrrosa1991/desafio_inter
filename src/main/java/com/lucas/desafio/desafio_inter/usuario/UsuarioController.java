package com.lucas.desafio.desafio_inter.usuario;

import java.util.List;
import java.util.Optional;

import com.lucas.desafio.desafio_inter.usuario.models.UsuarioModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping
    public List<UsuarioModel> list() {
        return usuarioRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<UsuarioModel> get(@PathVariable("id") String id) {
        return usuarioRepository.findById(Long.parseLong(id));
    }

    @PostMapping
    public UsuarioModel create(@RequestBody UsuarioModel usuario) {
        return usuarioRepository.save(usuario);
    }

    @PutMapping(value = "/{id}")
    public UsuarioModel update(@PathVariable("id") String id, @RequestBody UsuarioModel dto) {
        return usuarioRepository.save(dto);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        UsuarioModel usuario = usuarioRepository.getOne(id);
        usuarioRepository.delete(usuario);
        return;
    }

}
