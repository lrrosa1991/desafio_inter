package com.lucas.desafio.desafio_inter.usuario;

import com.lucas.desafio.desafio_inter.usuario.models.UsuarioModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioModel, Long> {

}
