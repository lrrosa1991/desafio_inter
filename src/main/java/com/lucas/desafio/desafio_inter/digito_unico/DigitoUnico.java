package com.lucas.desafio.desafio_inter.digito_unico;

import lombok.Data;

@Data
class DigitoUnico {
    private String n;
    private Integer k;
    private Integer resultado;
    private Long usuarioId;

    public void calculateResult() {
        this.resultado = CalculoDigitoUnico.getInstance().calculaDigitoUnico(this.n, this.k);

    }
}
