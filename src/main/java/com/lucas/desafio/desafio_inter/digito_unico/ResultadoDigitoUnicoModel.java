package com.lucas.desafio.desafio_inter.digito_unico;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.lucas.desafio.desafio_inter.usuario.models.UsuarioModel;

import lombok.Data;

/**
 * DigitoUnicoModel
 */
@Data
@Entity
public class ResultadoDigitoUnicoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String n;
    private Integer k;
    private Integer resultado;

    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = true)
    private UsuarioModel usuario;

}