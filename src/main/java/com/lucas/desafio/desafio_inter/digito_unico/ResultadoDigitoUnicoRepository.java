package com.lucas.desafio.desafio_inter.digito_unico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultadoDigitoUnicoRepository extends JpaRepository<ResultadoDigitoUnicoModel, Long> {

    public List<ResultadoDigitoUnicoModel> getResultadosByUsuarioId(Long usuarioId);
}
