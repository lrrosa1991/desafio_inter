package com.lucas.desafio.desafio_inter.digito_unico;

import java.util.ArrayList;
import java.util.List;

public class CalculoDigitoUnico {

    private static CalculoDigitoUnico instance;

    private List<DigitoUnico> cache = new ArrayList<DigitoUnico>();

    public static synchronized CalculoDigitoUnico getInstance() {
        if (instance == null)
            instance = new CalculoDigitoUnico();

        return instance;
    }

    private CalculoDigitoUnico() {
    }

    public Integer calculaDigitoUnico(String n, Integer k) {

        DigitoUnico cachedResultado = this.cache.stream()
                .filter(item -> (n.equals(item.getN()) && k.equals(item.getK()))).findAny().orElse(null);

        if (cachedResultado != null)
            return cachedResultado.getResultado();

        cachedResultado = new DigitoUnico();
        cachedResultado.setK(k);
        cachedResultado.setN(n);
        cachedResultado.setResultado(this.calculaDigitoUnico(n.repeat(k)));

        this.addResultadoToCache(cachedResultado);

        return cachedResultado.getResultado();
    }

    private void addResultadoToCache(DigitoUnico item) {
        this.cache.add(item);
        if (this.cache.size() > 10)
            this.cache.remove(0);
    }

    private Integer calculaDigitoUnico(String n) {
        Integer result = 0;
        for (String s : n.split("")) {
            result += Integer.parseInt(s);
        }

        if (result.toString().length() > 1) {
            return this.calculaDigitoUnico(result.toString());
        }

        return result;
    }
}
