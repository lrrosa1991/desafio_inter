package com.lucas.desafio.desafio_inter.digito_unico;

import java.util.List;

import com.lucas.desafio.desafio_inter.usuario.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/digito")
public class DigitoUnicoController {

    @Autowired
    ResultadoDigitoUnicoRepository resultadoDigitoUnicoRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @PostMapping
    public Integer calculaDigitoUnico(@RequestBody DigitoUnico dto) {

        dto.calculateResult();

        if (dto.getUsuarioId() != null) {
            ResultadoDigitoUnicoModel res = new ResultadoDigitoUnicoModel();

            res.setK(dto.getK());
            res.setN(dto.getN());
            res.setResultado(dto.getResultado());
            res.setUsuario(usuarioRepository.getOne(dto.getUsuarioId()));
            resultadoDigitoUnicoRepository.save(res);
        }
        return dto.getResultado();
    }

    @GetMapping("/get-by-users/{usuarioId}")
    public List<ResultadoDigitoUnicoModel> buscaResultadosPorUsuario(@PathVariable("usuarioId") String usuarioId) {
        return resultadoDigitoUnicoRepository.getResultadosByUsuarioId(Long.parseLong(usuarioId));
    }
}
